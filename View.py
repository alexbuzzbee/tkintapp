"""
Contains the View class.
"""
import tkinter as tk

class View:
  """
  Manages a Tkinter frame and allows it to be nested, windowed, or hidden. Handles some of the work of recreating frames when their parents change.

  _f is the View's frame.

  This class is not thread-safe.
  """
  _mode: str = "new"
  _title: str = "Untitled Window"
  _f: tk.Frame = None
  _win: tk.Toplevel = None
  _parent = None
  _parentView = None
  _children = None
  _childAs: int # Child number in parent.

  def title(self, title: str):
    """
    Sets the View's title, and that of its underlying frame.
    """
    self._title = title
    if self._f != None and self._mode == "window":
      self._f.master.title(title)

  def mode(self, mode: str):
    """
    Sets the View's mode:

    * window: Places the View in its own window.
    * nest: Nests the View inside its parent (you must pack/place it yourself).
    * hide: Hides the View.
    """
    new = self._mode == "new"
    self._mode = mode

    self.recreating()

    if self._f != None:
      self._f.destroy()
      del self._f

    if self._mode == "window":
      if self._win == None:
        self._win = tk.Toplevel()
      self._f = tk.Frame(master=self._win)
      self.widgets(new)
      self._f.master.title(self._title)
      self._win.protocol("WM_DELETE_WINDOW", self.destroy)
    elif self._mode == "nest":
      if self._win != None:
        self._win.destroy()
        del self._win
      self._f = tk.Frame(master=self._parent)
      self.widgets(new)

  def parent(self, parent, frameUpdate=False):
    """
    Sets the View's parent.

    frameUpdate is True if the parent is calling to update parent frame.
    """
    if parent is self:
      raise Exception("Cannot set a View as its own parent (this may lead to infinite recursion when it updates).")
    if isinstance(self._parentView, View) and not frameUpdate:
      self._parentView.removeChild(self._childAs)
    if isinstance(parent, View): # Handle parent being either a View or a Frame.
      self._parentView = parent
      self._parent = parent._f
      if not frameUpdate:
        self._childAs = parent.addChild(self)
    else:
      self._parentView = None
      self._parent = parent
    self.mode(self._mode)

  def addChild(self, child):
    if child is self:
      raise Exception("Cannot add a View to its own children (this may lead to infinite recursion when it updates).")
    num = len(self._children)
    self._children.append(child)
    return num

  def removeChild(self, child):
    del self._children[child]

  def destroy(self):
    """
    Destroys the View.
    """
    for child in self._children:
      child.destroy()
    self._f.destroy()
    if self._win != None:
      self._win.destroy()

  def recreating(self):
    """
    Called when the View is about to be recreated and its state needs to be saved (outside the widgets) so it can be reconstructed.
    """
    pass

  def widgets(self, new=False):
    """
    Creates and initializes the widgets in the View after its mode is changed/it is created. Should restore any saved states. Don't do any expensive computation here if it can be avoided; it may be called many times, depending on your application's structure.

    The default implementation handles rebuilding child Views and displaying the view.

    new is set if the View was just created.
    """
    for child in self._children:
      child.parent(self, frameUpdate=True)

  def __init__(self, mode="window", title="Untitled Window", parent=None):
    self._children = []
    self.mode(mode)
    self.parent(parent)
    self.title(title)
