"""
Contains a tear-off tab view.
"""
from tkinter import ttk
from ..View import View

class TabWrapper(View):
  name: str
  contained: View
  popBtn: ttk.Button
  popped: bool = False

  def pop(self):
    if self.popped:
      self.contained.mode("nest")
      self.popBtn.config(text="Tear off")
      self.popped = False
    else:
      self.contained.mode("window")
      self.popBtn.config(text="Reattach")
      self.popped = True

  def widgets(self, new=True):
    self.popBtn = ttk.Button(self._f, text="Tear off", command=self.pop)
    self.popBtn.pack()

    self._f.pack()
    super().widgets(new)
  
  def __init__(self, contained, name, *args, **kwargs):
    self.contained = contained
    self.name = name

    super().__init__(*args, **kwargs)

    self.contained.parent(self)

class TearoffTabView(View):
  widget: ttk.Notebook
  tabs = []

  def add(self, child, name):
    self.tabs.append(TabWrapper(child, name, mode="nest", parent=self.widget))

  def updateTabs(self):
    for tab in self.tabs:
      tab.parent(self.widget)
      self.widget.add(tab._f, text=tab.name)

  def widgets(self, new=True):
    self.widget = ttk.Notebook(self._f)
    self.widget.pack()
    self.updateTabs()

    self._f.pack()
    super().widgets(new)

  def __init__(self, *args, **kwargs):
    self.tabs = []

    super().__init__(*args, **kwargs)