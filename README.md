# Tkintapp

Tkintapp is a basic, very lightweight "framework" (one class with 9 methods and 8 properties, and one function) for Python GUI applications using Tkinter.

It organizes the application into "views," which are basically fancy frames managed by a derivable class. Views can be automatically made into a window, nested, or hidden. This allows implementing structures such as pop-out tab views; a tab is nested when selected, hidden when unselected, and windowed when popped out.
