#!/usr/local/bin/python3
"""
Demo application for tkintapp TearoffTabViews
"""
import tkinter as tk
import tkintapp
from tkintapp.View import View
from tkintapp.Views.TearoffTabView import TearoffTabView

class FirstView(View): # A very simple view.
  label: tk.Label

  def widgets(self, new=False):
    self.label = tk.Label(master=self._f, text="The first view.")
    self.label.pack()
    self._f.pack()

    super().widgets(new)

class SecondView(View): # Another very simple view
  label: tk.Label

  def widgets(self, new=False):
    self.label = tk.Label(master=self._f, text="The second view.")
    self.label.pack()
    self._f.pack()

    super().widgets(new)

class ContainerView(View): # A view containing the two others in a TearoffTabView.
  tabs: TearoffTabView

  def widgets(self, new=False):
    if new:
      self.tabs = TearoffTabView(mode="nest", parent=self)
      self.tabs.add(FirstView(mode="nest"), "First view")
      self.tabs.add(SecondView(mode="nest"), "Second view")
    
    self._f.pack()
    super().widgets(new)
  
  def destroy(self):
    super().destroy()
    self._f.quit()

tkintapp.startApp(ContainerView, "Tab demo application")