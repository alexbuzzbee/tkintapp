#!/usr/local/bin/python3
"""
Demo application for Tkintapp
"""
import tkinter as tk
from tkintapp.View import View
import tkintapp

class HelloView(View): # A very simple view.
  label: tk.Label

  def widgets(self, new=False):
    self.label = tk.Label(master=self._f, text="Hello, world!")
    self.label.pack()
    self._f.pack()

    super().widgets(new)

class MainView(View): # A more complex view that leverages Tkintapp on a HelloView.
  hello: HelloView
  hideBtn: tk.Button
  popoutBtn: tk.Button
  quitBtn: tk.Button
  hidden = False
  popped = False

  def hide(self):
    if self.hidden:
      self.hello.mode("nest")
      self.hidden = False
      self.hideBtn.config(text="Hide hello")
    else:
      self.hello.mode("hide")
      self.hidden = True
      self.hideBtn.config(text="Show hello")

  def popout(self):
    if self.popped:
      self.hello.mode("nest")
      self.popped = False
      self.hidden = False
      self.popoutBtn.config(text="Pop out hello")
    else:
      self.hello.mode("window")
      self.popped = True
      self.hidden = False
      self.popoutBtn.config(text="Pop in hello")

  def widgets(self, new=False):
    if new: # Don't create views repeatedly (it wastes memory and loses state).
      self.hello = HelloView(mode="nest", title="Hello", parent=self)
    self.hideBtn = tk.Button(master=self._f, text="Hide hello", command=self.hide)
    self.hideBtn.pack()

    self.popoutBtn = tk.Button(master=self._f, text="Pop out hello", command=self.popout)
    self.popoutBtn.pack()

    self.quitBtn = tk.Button(master=self._f, text="Quit", command=self._f.quit)
    self.quitBtn.pack()

    self._f.pack()

    super().widgets(new)

tkintapp.startApp(MainView, "Demo application")
