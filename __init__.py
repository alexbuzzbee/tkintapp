"""
Tkintapp is a lightweight application framework built on Tkinter.

Views are managed Tkinter frames that can be automatically nested in one another, hidden, or made into windows.
"""
import tkinter as tk
from .View import View

__all__ = ["View"]

def startApp(Class, title: str):
  """
  Starts the application using a specified class for the initial view and window title.
  """
  root = tk.Tk()
  view = Class(title=title)
  root.withdraw()
  view._f.mainloop()
